module main
variables e h 'h.' q 'q.'

	spec ' ' 'part' 'part1'

	spec ' ' 'part2' 'part2'

	spec ' ' 'part3' 'part3'

	spec ' ' 'part4' 'part4'


to part {
  waitMillis q
  'play tone' 'B' 0 q
  'play tone' 'E' 1 q.
  'play tone' 'G' 1 e
  'play tone' 'F#' 1 q
  'play tone' 'E' 1 h
  'play tone' 'B' 1 q
  'play tone' 'A' 1 h.
  'play tone' 'F#' 1 h.
  'play tone' 'E' 1 q.
  'play tone' 'G' 1 e
  'play tone' 'F#' 1 q
  'play tone' 'D' 1 h
  'play tone' 'F' 1 q
  'play tone' 'B' 0 h.
}

to part2 {
  waitMillis q
  'play tone' 'B' 0 q
  'play tone' 'E' 1 q.
  'play tone' 'G' 1 e
  'play tone' 'F#' 1 q
  'play tone' 'E' 1 h
  'play tone' 'B' 1 q
  'play tone' 'D' 2 h
  'play tone' 'C#' 2 q
  'play tone' 'C' 2 h
  'play tone' 'G#' 1 q
  'play tone' 'C' 2 q.
  'play tone' 'B' 1 e
  'play tone' 'A#' 1 q
  'play tone' 'F#' 1 h
  'play tone' 'G' 1 q
  'play tone' 'E' 1 h.
}

to part3 {
  waitMillis q
  'play tone' 'E' 1 q
  'play tone' 'G' 1 q
  'play tone' 'B' 1 h
  'play tone' 'G' 1 q
  'play tone' 'B' 1 h
  'play tone' 'G' 1 q
  'play tone' 'C' 2 h
  'play tone' 'B' 1 q
  'play tone' 'A#' 1 h
  'play tone' 'F#' 1 q
  'play tone' 'G' 1 q.
  'play tone' 'B' 1 e
  'play tone' 'A#' 1 q
  'play tone' 'A#' 0 h
  'play tone' 'B' 0 q
  'play tone' 'B' 1 h.
}

to part4 {
  waitMillis q
  'play tone' 'E' 1 q
  'play tone' 'G' 1 q
  'play tone' 'B' 1 h
  'play tone' 'G' 1 q
  'play tone' 'B' 1 h
  'play tone' 'G' 1 q
  'play tone' 'D' 2 h
  'play tone' 'C#' 2 q
  'play tone' 'C' 2 h
  'play tone' 'G#' 1 q
  'play tone' 'C' 2 q.
  'play tone' 'B' 1 e
  'play tone' 'A#' 1 q
  'play tone' 'F#' 1 h
  'play tone' 'G' 1 q
  'play tone' 'E' 1 h.
}

script 51 50 {
whenStarted
comment 'To hear the music, attach a piezo speaker between pin 0 and ground'
comment 'On the Circuit Playground Express or ED1, it will use the built-in speaker.'
e = 150
q = (2 * e)
q. = (3 * e)
h = (4 * e)
h. = (6 * e)
w = (8 * e)
part
part2
part3
part4
}

script 270 153 {
to part {}
}

script 583 153 {
to part2 {}
}

script 273 647 {
to part4 {}
}

script 585 704 {
to part3 {}
}


module Tone Output
variables _tonePin _toneInitalized _toneLoopOverhead

	spec ' ' 'play tone' 'play note _ octave _ during _ ms' 'auto num num' 'C' 0 500
	spec ' ' 'play frequency' 'play frequency _ for _ milliseconds' 'num num' 261 500
	spec ' ' 'playMIDIKey' 'play midi key _ for _ milliseconds' 'num num' 60 500
	spec ' ' 'attach buzzer to pin' 'attach buzzer to pin _' 'auto' ''
	spec 'r' '_measureLoopOverhead' '_measureLoopOverhead'
	spec 'r' '_baseFreqForNote' '_baseFreqForNote _' 'auto' 'c'
	spec 'r' '_baseFreqForSharpOrFlat' '_baseFreqForSharpOrFlat _' 'auto' 'c#'
	spec 'r' '_baseFreqForSemitone' '_baseFreqForSemitone _' 'num' 0
	spec ' ' '_toneLoop' '_toneLoop _ for _ milliseconds' 'num num' 440000 100

to '_baseFreqForNote' note {
  comment 'Return the frequency for the given note in the middle-C octave
scaled by 1000. For example, return 440000 (440Hz) for A.
Note names may be upper or lower case. Note names
may be followed by # for a sharp or b for a flat.'
  if (or (note == 'c') (note == 'C')) {
    return 261626
  } (or (note == 'd') (note == 'D')) {
    return 293665
  } (or (note == 'e') (note == 'E')) {
    return 329628
  } (or (note == 'f') (note == 'F')) {
    return 349228
  } (or (note == 'g') (note == 'G')) {
    return 391995
  } (or (note == 'a') (note == 'A')) {
    return 440000
  } (or (note == 'b') (note == 'B')) {
    return 493883
  }
  return ('_baseFreqForSharpOrFlat' note)
}

to '_baseFreqForSemitone' semitone {
  if (0 == semitone) {
    return 261626
  } (1 == semitone) {
    return 277183
  } (2 == semitone) {
    return 293665
  } (3 == semitone) {
    return 311127
  } (4 == semitone) {
    return 329628
  } (5 == semitone) {
    return 349228
  } (6 == semitone) {
    return 369994
  } (7 == semitone) {
    return 391995
  } (8 == semitone) {
    return 415305
  } (9 == semitone) {
    return 440000
  } (10 == semitone) {
    return 466164
  } (11 == semitone) {
    return 493883
  }
}

to '_baseFreqForSharpOrFlat' note {
  comment 'Return the frequency for the given sharp or flat note in the
middle-C octave scaled by 1000. Only handles black keys.
Thus, you can''t write E# to mean F.'
  if (or (or (note == 'c#') (note == 'C#')) (or (note == 'db') (note == 'Db'))) {
    return 277183
  } (or (or (note == 'd#') (note == 'D#')) (or (note == 'eb') (note == 'Eb'))) {
    return 311127
  } (or (or (note == 'f#') (note == 'F#')) (or (note == 'gb') (note == 'Gb'))) {
    return 369994
  } (or (or (note == 'g#') (note == 'G#')) (or (note == 'ab') (note == 'Ab'))) {
    return 415305
  } (or (or (note == 'a#') (note == 'A#')) (or (note == 'bb') (note == 'Bb'))) {
    return 466164
  }
  comment 'Unrecognized note names map to 0.1 Hz, which is inaudible.
This helps users find typos in their tunes.'
  return 100
}

to '_measureLoopOverhead' {
  comment 'Measure the loop overhead on this device'
  '[display:mbDisplayOff]'
  local 'halfCycle' 100
  local 'startT' (microsOp)
  repeat 100 {
    digitalWriteOp _tonePin false
    waitMicros halfCycle
    digitalWriteOp _tonePin false
    waitMicros halfCycle
  }
  local 'usecs' ((microsOp) - startT)
  return ((usecs - 20000) / 200)
}

to '_toneLoop' scaledFreq ms {
  if (_toneInitalized == 0) {'attach buzzer to pin' ''}
  if ('[io:hasTone]') {
    '[io:playTone]' _tonePin (scaledFreq / 1000)
    waitMillis ms
    '[io:playTone]' _tonePin 0
  } else {
    local 'halfCycle' ((500000000 / scaledFreq) - _toneLoopOverhead)
    local 'cycles' ((ms * 500) / halfCycle)
    repeat cycles {
      digitalWriteOp _tonePin true
      waitMicros halfCycle
      digitalWriteOp _tonePin false
      waitMicros halfCycle
    }
  }
}

to 'attach buzzer to pin' pinNumber {
  if (pinNumber == '') {
    comment 'Pin number not specified; use default pin for this device'
    if ((boardType) == 'Citilab ED1') {
      _tonePin = 26
    } ((boardType) == 'CircuitPlayground') {
      _tonePin = 12
    } else {
      _tonePin = 0
    }
  } else {
    _tonePin = pinNumber
  }
  _toneLoopOverhead = ('_measureLoopOverhead')
  _toneInitalized = (booleanConstant true)
}

to 'play frequency' freq ms {
  '_toneLoop' (freq * 1000) ms
}

to 'play tone' note octave ms {
  local 'freq' ('_baseFreqForNote' note)
  if (octave < 0) {
    repeat (absoluteValue octave) {
      freq = (freq / 2)
    }
  }
  repeat octave {
    freq = (freq * 2)
  }
  '_toneLoop' freq ms
}

to playMIDIKey key ms {
  local 'freq' ('_baseFreqForSemitone' (key % 12))
  local 'octave' ((key / 12) - 5)
  if (octave < 0) {
    repeat (absoluteValue octave) {
      freq = (freq / 2)
    }
  }
  repeat octave {
    freq = (freq * 2)
  }
  '_toneLoop' freq ms
}

