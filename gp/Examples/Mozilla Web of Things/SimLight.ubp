module main
variables _useWiFi 'circle-x' 'circle-y' 'label-x' 'label-y' last_on 'light-color' on

	spec ' ' 'redraw_display' 'redraw_display' 'any'

to redraw_display {
  if on {
    '[tft:circle]' circle-x circle-y 30 light-color true
    '[tft:text]' 'Light OFF' label-x label-y 0 2 false
    '[tft:text]' 'Light ON' label-x label-y 16777215 2 false
  } else {
    '[tft:circle]' circle-x circle-y 30 0 true
    '[tft:circle]' circle-x circle-y 30 light-color false
    '[tft:text]' 'Light ON' label-x label-y 0 2 false
    '[tft:text]' 'Light OFF' label-x label-y 16777215 2 false
  }
  waitMillis 100
}

script 466 50 {
comment 'toggle test'
on = (not on)
}

script 54 58 {
whenStarted
'wifi connect to' 'Network_Name' '' 3
defineThing 'Circle Light' 'Light'
addBooleanProperty 'Circle Light' 'on' 'OnOffProperty'
'[tft:enableDisplay]' true
comment 'IoT-Bus TFT is 320 x 240'
on = (booleanConstant false)
last_on = (booleanConstant true)
circle-x = 160
circle-y = 120
label-x = 115
label-y = 180
light-color = ('_tft color' (colorSwatch 255 255 255 255))
'[tft:text]' (getIPAddress) 30 10 16777215 2 false
'[tft:text]' 'Simulated Light' 20 40 16777215 3 false
'[tft:circle]' circle-x circle-y 30 0 true
'[tft:circle]' circle-x circle-y 30 light-color false
forever {
  if (on != last_on) {
    redraw_display
    last_on = on
  }
}
}

script 523 146 {
'[tft:enableDisplay]' true
}

script 525 211 {
'[display:mbDisplayOff]'
}

script 544 267 {
to redraw_display {}
}


module TFT Output

	spec ' ' '[tft:enableDisplay]' 'enable TFT _' 'bool' true
	spec ' ' 'set TFT pixel x' 'set TFT pixel x _ y _ to _' 'auto auto color' '10' '10'
	spec ' ' '[tft:line]' 'draw line on TFT from x _ y _ to x _ y _ color _' 'num num num num num' 12 8 25 15 255
	spec ' ' '[tft:rect]' 'draw rectangle on TFT at x _ y _ width _ height _ color _ : filled _' 'num num num num num bool' 10 10 40 30 65280 false
	spec ' ' '[tft:roundedRect]' 'draw rounded rectangle on TFT at x _ y _ width _ height _ radius _ color _ : filled _' 'num num num num num num bool' 10 10 40 30 8 12255317 false
	spec ' ' '[tft:circle]' 'draw circle on TFT at x _ y _ radius _ color _ : filled _' 'num num num num bool' 60 100 30 65535 false
	spec ' ' '[tft:triangle]' 'draw triangle on TFT at x _ y _ , x _ y _ , x _ y _ color _ : filled _' 'num num num num num num num bool' 20 20 30 80 60 5 5592354 false
	spec ' ' '[tft:text]' 'write _ on TFT at x _ y _ color _ : scale _ wrap _' 'str num num num num bool' 'Hello World!' 0 80 16777215 1 false
	spec ' ' '[tft:setPixel]' 'set TFT pixel x _ y _ to _' 'num num num' 50 32 16711680
	spec 'r' '_tft color' '_tft color _ to number' 'color any'

to '_tft color' 'color hex' {
  local 'r' (((((v 'color hex') >> 16) & 255) * 255) / 31)
  local 'g' (((((v 'color hex') >> 8) & 255) * 255) / 31)
  local 'b' ((((v 'color hex') & 255) * 255) / 31)
  return ((r << 16) | ((g << 8) | b))
}

to 'set TFT pixel x' x y color {
  '[tft:setPixel]' x y ('_tft color' color)
}


module 'Web of Things' Comm
variables _useWiFi

	spec ' ' 'wifi connect to' 'wifi connect to _ password _ try _ times' 'str str num' 'Network_Name' '' 3
	spec ' ' 'wifiCreateHotspot' 'wifi create hotspot _ password _' 'str str' 'Network_Name' 'Network_Password'
	spec 'r' 'getIPAddress' 'IP address'
	spec ' ' 'defineThing' 'define thing _ capability _' 'str str.thingTypesMenu' 'Hello LED' 'Light'
	spec ' ' 'registerEvent' 'register event _ type _' 'str str.thingEventTypesMenu' 'Button Pressed' 'PressedEvent'
	spec ' ' 'addBooleanProperty' 'add boolean property title _ variable _ @Type _' 'str menu.allVarsMenu str.booleanPropertyTypesMenu' '' '' 'OnOffProperty'
	spec ' ' 'addNumProp' 'add number property title _ variable _ min _ max _ @Type _' 'str menu.allVarsMenu num num str.numberPropertyTypesMenu' '' '' 0 100 'LevelProperty'
	spec ' ' 'addStringProperty' 'add string property title _ variable _ @Type _' 'str menu.allVarsMenu str.stringPropertyTypesMenu' '' '' 'ColorProperty'
	spec ' ' '_addCustomProperty' '_add to last property key _ value _' 'str auto' '' '"json value"'
	spec 'r' '[net:thingDescription]' 'thing description'
	spec ' ' '[net:clearThingDescription]' 'clear thing description'

to '_addCustomProperty' key jsonValue {
  if _useWiFi {'[net:appendToThingProperty]' '"' key '": ' jsonValue}
}

to addBooleanProperty title var typeName {
  if _useWiFi {
    '[net:appendToThingDescription]' '    "' var '": {"title": "' title '", "type": "boolean", "href": "/properties/' var '", "@type": "' typeName '"},'
  } else {
    sendBroadcast 'moz-property {"title": "' title '", "type": "boolean", "href": "/properties/' var '", "@type": "' typeName '"}'
  }
}

to addNumProp title var min max typeName {
  if _useWiFi {
    '[net:appendToThingDescription]' '    "' var '": {"title": "' title '", "type": "number", "minimum": ' min ', "maximum": ' max ', "href": "/properties/' var '", "@type": "' typeName '"'
    if ('TemperatureProperty' == typeName) {
      '[net:appendToThingDescription]' ', "unit": "degree celsius"'
    }
    '[net:appendToThingDescription]' '},'
  } else {
    sendBroadcast 'moz-property {"title":"' title '", "type": "number", "minimum": ' min ', "maximum": ' max ', "href": "/properties/' var '", "@type": "' typeName '"}'
  }
}

to addStringProperty title var typeName {
  if _useWiFi {
    '[net:appendToThingDescription]' '    "' var '": {"title": "' title '", "type": "string", "href": "/properties/' var '", "@type": "' typeName '"},'
  } else {
    sendBroadcast 'moz-property {"title":"' title '", "type": "string", "href": "/properties/' var '", "@type": "' typeName '"}'
  }
}

to defineThing name capability {
  _useWiFi = ('[net:hasWiFi]')
  if _useWiFi {
    '[net:clearThingDescription]'
    if ('' == capability) {
      '[net:appendToThingDescription]' '{ "name": "' name '",
  "@type": [ ],
  "properties": {'
    } else {
      '[net:appendToThingDescription]' '{ "name": "' name '",
  "@context": "https://iot.mozilla.org/schemas/",
  "@type": ["' capability '"],
  "properties": {'
    }
  } else {
    sendBroadcast 'moz-thing { "name": "' name '","@context": "https://iot.mozilla.org/schemas/","@type": ["' capability '"]}'
  }
}

to getIPAddress {
  return ('[net:myIPAddress]')
}

to registerEvent title type {
  if _useWiFi {
    sayIt 'Not yet supported'
  } else {
    sendBroadcast 'moz-event {"name":"' title '", "metadata":{"description":"MicroBlocks event", "@type":"' type '"}}'
  }
}

to 'wifi connect to' ssid password tries {
  if (not ('[net:hasWiFi]')) {return}
  repeatUntil (or ('Connected' == ('[net:wifiStatus]')) (tries < 1)) {
    '[net:startWiFi]' ssid password
    repeatUntil ('Trying...' != ('[net:wifiStatus]')) {
      comment 'Slow blink while connecting'
      setUserLED true
      waitMillis 500
      setUserLED false
      waitMillis 500
    }
    tries += -1
  }
  repeat 8 {
    comment 'Quick blinks when connected'
    setUserLED true
    waitMillis 50
    setUserLED false
    waitMillis 50
  }
  sayIt 'My IP address is:' ('[net:myIPAddress]')
}

to wifiCreateHotspot ssid password {
  '[net:startWiFi]' ssid password true
}

